# AngularPerformance

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.13.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Deploy a demo
Run `npm run demo:deploy demo-name` (e.g `npm run demo:deploy jay-661-empty-project`)

## Test Performance
Run `npm run lighthouse http://url` to generate Lighthouse performance report. 
To analyse the report upload `test.json` from `dist` folder to [https://googlechrome.github.io/lighthouse/viewer/](https://googlechrome.github.io/lighthouse/viewer/)

## NOTES
TS configuration updated to be hosted on AWS
"target": "es2015" -> "target": "es5"
You can roll it back when hosting on a web-server, so different assets will be generated for new and old browsers.