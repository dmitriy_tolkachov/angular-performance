/**
 * This module used to set cpecific credential to run all commands. 
 * Credentials will be set as a profile in AWS config file. 
 * More to read http://docs.aws.amazon.com/cli/latest/reference/configure/set.html
 * @name set-credentials
 */

var shell = require('shelljs');

const awsConfig = require('./aws-config.json');
const utils = require('./utils');

/**
 * @property {string} credentialsProfileName the name of profile which credentials will be used to run all the commands
 * @memberof set-credentials
 */
const credentialsProfileName = awsConfig.aws_profile_name;

/**
 * @method setCredentials
 * @memberof set-credentials
 */
function setCredentials() {
  checkIfAwsConfigExists();

  let addCredIdExecCode = shell.exec('aws configure set aws_access_key_id ' + awsConfig.aws_access_key_id + ' --profile ' + credentialsProfileName, utils.cmdOptions).code;
  let addCredSecretExecCode = shell.exec('aws configure set aws_secret_access_key ' + awsConfig.aws_secret_access_key + ' --profile ' + credentialsProfileName, utils.cmdOptions).code;
  let addCredRegionExecCode = shell.exec('aws configure set region ' + awsConfig.region + ' --profile ' + credentialsProfileName, utils.cmdOptions).code;

  if(addCredIdExecCode || addCredSecretExecCode || addCredRegionExecCode) {
    utils.log('An Error happened when setting AWS deploy credentials');
  } else {
    utils.log('Credentials were placed to AWS config to profile [' + credentialsProfileName + ']');
  }
}

/**
 * @method checkIfAwsConfigExists
 * @private 
 * @memberof set-credentials
 */
function checkIfAwsConfigExists(){
  if (!awsConfig || !awsConfig.aws_access_key_id || !awsConfig.aws_secret_access_key || !awsConfig.region) {
    utils.log('This script requires `aws-config.json` with credentials for aws cli');
    shell.exit(1);
  }
}

module.exports = {
  setCredentials: setCredentials,
  credentialsProfile: credentialsProfileName
};
