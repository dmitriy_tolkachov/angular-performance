# DEMO DEPLOY COMMAND LINE TOOL

This is a simple command line tool that allows to manage progect demos by deploying/removeing builds of your app to S3.

## Dependencies
- aws cli http://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html
- shelljs https://www.npmjs.com/package/shelljs

## How to install
- place this folder to a desired place inside your project
- open `package.json` file and set a bundle of new commands:
    * `"demo:list": "node [path_to_folder]/demo-deploy/get-all.js",`
    * `"demo:deploy": [path_to_folder]/demo-deploy/deploy.js",`
    * `"demo:remove": [path_to_folder]/demo-deploy/remove.js",`

## How to use
`npm run demo:list` - get list of all demo buckets

`npm run demo:remove jws-197` - remove specific demo bucket

`npm run demo:remove` - remove all demo buckets

`npm run demo:deploy jws-197` - make a build on currenct code state with UAT environment configuration and deploy to S3 

## Notes
This tool will create a credentials profile for aws cli called `deploy-demo`. You can set your credentials data in `aws-config.json`

This tool run command 'npm run build:uat' to create build output that will be uploaded to Amazon S3.

It also assumes that build output will be placed to /dist folder.

If you have different settings in you project feel free to updated respective values in demo-deploy-tool/utils