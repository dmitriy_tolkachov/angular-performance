/**
 * node script that returns/shows array of demo S3 buckets
 * @name get-all
 * @example 
 * //direct command
 * node config/demo-deploy-tool/get-all.js
 * 
 * //npm command
 * npm run demo
 */

var shell = require('shelljs');
const credentials = require('./set-credentials');
const utils = require('./utils');

/**
 * @method getAllDemoBuckets
 * @memberof get-all
 * @returns {array} list of demo buckets
 */
function getAllDemoBuckets() {
  var buckets = shell.exec('aws s3 ls --profile ' + credentials.credentialsProfile, utils.cmdOptions).stdout;
  const regex = /demo-.+/g;
  let demoBuckets = buckets.match(regex);

  return demoBuckets || null;
}

/**
 * @method getAllDemoBucketsNow
 * @memberof get-all
 * @private 
 */
function getAllDemoBucketsNow() {
  utils.checkAwsCliExists();
  credentials.setCredentials();
  let demoBuckets = getAllDemoBuckets();

  utils.log('Currently deployed following demos: \n' + (demoBuckets ? demoBuckets.join('\n') : 'no demo buckets deployed.'));
}

//if this file execured directly from command line - run it's logic, if imported by another module - just export some functionality
if (module.parent) { //module imported by another module (via `require`)
  module.exports = {
    getAllDemoBuckets: getAllDemoBuckets
  };
} else { //module execured directly (via `npm run`)
  getAllDemoBucketsNow();
}
