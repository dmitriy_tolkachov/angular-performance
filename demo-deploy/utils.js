/**
 * This module used as a helper to contain properties and methods that can be used in enother modules
 * @name utils
 */

var shell = require('shelljs');

/**
 * @property {object} cmdOptions options for executiong cmd command
 * @memberof utils
 */
const cmdOptions = {
  silent: true
};

/**
 * @method checkAwsCliExists
 * @memberof unitls
 */
function checkAwsCliExists() {
  if (!shell.which('aws')) {
    shell.echo('This script requires aws cli tool to be installed and configured.');
    shell.echo('Instalation instructions: http://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html#install-msi-on-windows');
    shell.echo('Configuration instructions: http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-using-examples');
    shell.exit(1);
  }
}

/**
 * output meesage (formatted) into console
 * @param {string} msg meessage to log into console
 */
function log(msg) {
  shell.echo('=======================================================================');
  shell.echo(msg);
  shell.echo('=======================================================================');
}

module.exports = {
  checkAwsCliExists: checkAwsCliExists,
  cmdOptions: cmdOptions,
  log: log
}
