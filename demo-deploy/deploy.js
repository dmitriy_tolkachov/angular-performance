/**
 * node script that can deploy a demo of currenct app state to a S3 bucket
 * @name deploy
 * @example 
 * //direct command
 * node config/demo-deploy-tool/deploy.js --name jws-198 
 * node config/demo-deploy-tool/deploy.js jws-198 
 * 
 * //npm command
 * npm run demo:deploy --name jws-198
 * npm run demo:deploy jws-198
 * @returns url string to a demo
 */

const shell = require('shelljs');
const utils = require('./utils');
const credentials = require('./set-credentials');
const config = require('./config.json');

/**
 * @method deployDemoNow
 * @private 
 * @memberof deploy
 */
function deployDemoNow() {
  utils.checkAwsCliExists();

  const refName = process.argv[2];
  if (!refName) {
    utils.log('Sorry, this script requires reference to a JIRA ticket (e.g. "npm run demo:deploy --name jws-198", where "jws-198" is a reference name)');
    shell.exit(1);
  }

  utils.log(`Creating a demo for ${refName}. This may take some time.`);

  const bucketName = 'demo-' + refName.toLowerCase();

  //make a build
  let buildExecCode = shell.exec(config.builCommand).code;

  if (buildExecCode) {
    utils.log('There was an error when building the project. Please fix it first and run this script again.');
    shell.exit(1);
  }

  credentials.setCredentials();
  //create a S3 bucket
  let createBucketExecCode = shell.exec('aws s3 mb s3://' + bucketName + ' --profile ' + credentials.credentialsProfile, utils.cmdOptions).code;
  //deploy build to S3
  let deployExecCode = shell.exec('aws s3 sync ' + config.pathToBuildFolder + ' s3://' + bucketName + '/ --acl public-read --profile ' + credentials.credentialsProfile).code;
  //create static website on S3 bucket
  let createWebsiteExecCode = shell.exec('aws s3 website s3://' + bucketName + '/ --index-document index.html --error-document index.html --profile ' + credentials.credentialsProfile, utils.cmdOptions).code;

  if (deployExecCode || createWebsiteExecCode) {
    utils.log(`An Error happened while operating with S3. Please check logs above.
      creating S3 busket: ${createBucketExecCode},
      syncing files: ${deployExecCode},
      setting up website: ${createWebsiteExecCode}
    `);
    shell.exit(1);
  } else {
    utils.log(`Demo has been created at: http://${bucketName}.s3-website-ap-southeast-2.amazonaws.com`);
  }
}

deployDemoNow();
