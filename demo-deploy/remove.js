/**
 * node script that can delete defined (by JIRA reference name) or all S3 buckets
 * @name remove
 * @example 
 * //direct command
 * ////delete demo-jws-198 bucket
 * node config/demo-deploy-tool/remove.js --name jws-198 
 * node config/demo-deploy-tool/remove.js jws-198 
 * ////delete all demo buckets
 * node config/demo-deploy-tool/remove.js
 * 
 * //npm command
 * ////delete demo-jws-198 bucket
 * npm run demo:remove --name jws-198
 * npm run demo:remove jws-198
 * // delete multipple demos
 * npm run demo:remove jws-198 jws-199 jws-200
 * ////delete all demo buckets
 * npm run demo:remove
 */

const shell = require('shelljs');
const utils = require('./utils');
const credentials = require('./set-credentials');
const demoList = require('./get-all');

/**
 * @method removeBucketsNow
 * @private 
 * @memberof remove
 */
function removeBucketsNow() {
  utils.checkAwsCliExists();
  credentials.setCredentials();

  const refNames = process.argv.slice(2);
  if (refNames) { // remove 1 or a few demos
    refNames.forEach(refName => {
      //remove specific demo website
      const bucketName = 'demo-' + refName;

      utils.log('Deleting bucket ' + bucketName);

      removeBucketByName(bucketName);
    });
  } else { //remove all demo websites
    let demoBuckets = demoList.getAllDemoBuckets();

    if (!demoBuckets) {
      utils.log('There are no demo buckets.');
      return;
    }

    utils.log('deleting all demo buckets ', demoBuckets);

    demoBuckets.forEach((bucketName) => {
      removeBucketByName(bucketName);
    });
  }
}
/**
 * @method removeBucketByName
 * @memberof remove
 * @private 
 * @param {string} bucketName 
 */
function removeBucketByName(bucketName) {
  let removeBucketExecCode = shell.exec('aws s3 rb s3://' + bucketName.toLowerCase() + ' --force --profile ' + credentials.credentialsProfile, utils.cmdOptions).code;

  utils.log(removeBucketExecCode ? `Can't remove bucket ${bucketName}` : `Bucket ${bucketName} has been removed.`);
}

removeBucketsNow();